package com.devcamp.provinceapi.services;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.devcamp.provinceapi.models.Ward;
import com.devcamp.provinceapi.repository.WardRepository;

@Service
public class WardService {
    private final WardRepository wardRepository;

    public WardService(WardRepository wardRepository) {
        this.wardRepository = wardRepository;
    }

    public List<Ward> getWards(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Ward> wardPage = wardRepository.findAll(pageable);
        return wardPage.getContent();
    }

    public List<Ward> getWardByDistrict(int page, int size, int id) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Ward> wardPage = wardRepository.findByDistrictId(id, pageable);
        return wardPage.getContent();
    }

    public Ward createWard(Ward ward) {
        return wardRepository.save(ward);
    }

    public Ward updateWard(int id, Ward ward) {
        Optional<Ward> optionalWard = wardRepository.findById(id);
        if (optionalWard.isPresent()) {
            Ward existingWard = optionalWard.get();
            existingWard.setName(ward.getName());
            existingWard.setPrefix(ward.getPrefix());
            existingWard.setDistrict(ward.getDistrict());

            return wardRepository.save(existingWard);
        } else {
            return null;
        }
    }

    public void deleteWard(int id) {
        wardRepository.deleteById(id);
    }
}

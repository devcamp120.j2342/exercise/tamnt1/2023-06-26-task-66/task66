package com.devcamp.provinceapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.Province;
import com.devcamp.provinceapi.repository.ProvinceRepository;

@Service
public class ProvinceService {
    private final ProvinceRepository provinceRepository;

    public ProvinceService(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    public List<Province> getProvinces(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Province> provincePage = provinceRepository.findAll(pageable);
        return provincePage.getContent();
    }

    public Province getProvinceById(int id) {
        Optional<Province> optionalProvince = provinceRepository.findById(id);
        if (optionalProvince.isPresent()) {
            return optionalProvince.get();
        } else {
            return null;
        }
    }

    public List<Province> findByName(String name) {
        return provinceRepository.findByNameIs(name);
    }

    public Province createProvince(Province province) {
        return provinceRepository.save(province);
    }

    public Province updateProvince(int id, Province province) {
        Optional<Province> optionalProvince = provinceRepository.findById(id);
        if (optionalProvince.isPresent()) {
            Province existingProvince = optionalProvince.get();
            existingProvince.setName(province.getName());
            existingProvince.setCode(province.getCode());
            existingProvince.setDistricts(province.getDistricts());

            return provinceRepository.save(existingProvince);
        } else {
            return null;
        }
    }

    public void deleteProvince(int id) {
        provinceRepository.deleteById(id);
    }
}

package com.devcamp.provinceapi.services;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.District;
import com.devcamp.provinceapi.repository.DistrictRepository;

@Service
public class DistrictService {
    private final DistrictRepository districtRepository;

    public DistrictService(DistrictRepository districtRepository) {
        this.districtRepository = districtRepository;
    }

    public List<District> getDistricts(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<District> districtPage = districtRepository.findAll(pageable);
        return districtPage.getContent();
    }

    public List<District> getDistrictsByProvince(int page, int size, int provinceId) {
        Pageable pageable = PageRequest.of(page, size);
        Page<District> districtPage = districtRepository.findByProvinceId(provinceId, pageable);
        return districtPage.getContent();
    }

    public District getDistrictById(int id) {
        Optional<District> optionalDistrict = districtRepository.findById(id);
        if (optionalDistrict.isPresent()) {
            return optionalDistrict.get();
        } else {
            return null;
        }
    }

    public District createDistrict(District district) {
        return districtRepository.save(district);
    }

    public District updateDistrict(int id, District district) {
        Optional<District> optionalDistrict = districtRepository.findById(id);
        if (optionalDistrict.isPresent()) {
            District existingDistrict = optionalDistrict.get();
            existingDistrict.setName(district.getName());
            existingDistrict.setProvince(district.getProvince());
            existingDistrict.setWards(district.getWards());
            existingDistrict.setPrefix(district.getPrefix());

            return districtRepository.save(existingDistrict);
        } else {
            return null;
        }
    }

    public void deleteDistrict(int id) {
        districtRepository.deleteById(id);
    }
}

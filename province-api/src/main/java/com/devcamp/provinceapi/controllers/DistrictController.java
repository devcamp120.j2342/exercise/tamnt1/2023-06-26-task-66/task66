package com.devcamp.provinceapi.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.District;
import com.devcamp.provinceapi.services.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DistrictController {
    private final DistrictService districtService;

    public DistrictController(DistrictService districtService) {
        this.districtService = districtService;
    }

    @GetMapping("/district-list")
    public ResponseEntity<List<District>> getDistrictList(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<District> districtList = districtService.getDistricts(page, size);
            return new ResponseEntity<>(districtList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district/{id}")
    public ResponseEntity<List<District>> getDistrictListByProvinceId(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @PathVariable("id") int id) {
        try {
            List<District> districtList = districtService.getDistrictsByProvince(page, size, id);
            return new ResponseEntity<>(districtList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district")
    public ResponseEntity<District> getDistricById(
            @RequestParam("id") int id) {
        try {
            District district = districtService.getDistrictById(id);
            return new ResponseEntity<>(district, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/district")
    public ResponseEntity<District> createDistrict(@RequestBody District district) {
        try {
            District newDistrict = districtService.createDistrict(district);
            return new ResponseEntity<>(newDistrict, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/district/{id}")
    public ResponseEntity<District> updateDistrict(@PathVariable("id") int id, @RequestBody District district) {
        try {
            District updatedDistrict = districtService.updateDistrict(id, district);
            if (updatedDistrict != null) {
                return new ResponseEntity<>(updatedDistrict, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/district/{id}")
    public ResponseEntity<HttpStatus> deleteDistrict(@PathVariable("id") int id) {
        try {
            districtService.deleteDistrict(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

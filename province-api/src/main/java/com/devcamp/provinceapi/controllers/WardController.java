package com.devcamp.provinceapi.controllers;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.provinceapi.models.Ward;
import com.devcamp.provinceapi.services.WardService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class WardController {
    private final WardService wardService;

    public WardController(WardService wardService) {
        this.wardService = wardService;
    }

    @GetMapping("/ward-list")
    public ResponseEntity<List<Ward>> getWardList(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<Ward> wardList = wardService.getWards(page, size);
            return new ResponseEntity<>(wardList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/ward/{id}")
    public ResponseEntity<List<Ward>> getWardListByDistrictId(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @PathVariable("id") int id) {
        try {
            List<Ward> wardList = wardService.getWardByDistrict(page, size, id);
            return new ResponseEntity<>(wardList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/ward")
    public ResponseEntity<Ward> createWard(@RequestBody Ward ward) {
        try {
            Ward newWard = wardService.createWard(ward);
            return new ResponseEntity<>(newWard, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/ward/{id}")
    public ResponseEntity<Ward> updateWard(@PathVariable("id") int id, @RequestBody Ward ward) {
        try {
            Ward updatedWard = wardService.updateWard(id, ward);
            if (updatedWard != null) {
                return new ResponseEntity<>(updatedWard, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/ward/{id}")
    public ResponseEntity<HttpStatus> deleteWard(@PathVariable("id") int id) {
        try {
            wardService.deleteWard(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

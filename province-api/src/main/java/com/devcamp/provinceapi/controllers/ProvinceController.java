package com.devcamp.provinceapi.controllers;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.Province;
import com.devcamp.provinceapi.services.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ProvinceController {
    private final ProvinceService provinceService;

    public ProvinceController(ProvinceService provinceService) {
        this.provinceService = provinceService;
    }

    @GetMapping("/province-list")
    public ResponseEntity<List<Province>> getProvinceList(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<Province> provinceList = provinceService.getProvinces(page, size);
            return new ResponseEntity<>(provinceList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province")
    public ResponseEntity<List<Province>> getProvinceByName(
            @RequestParam(value = "name", defaultValue = "") String name) {
        try {
            List<Province> provinceList = provinceService.findByName(name);
            return new ResponseEntity<>(provinceList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province/{id}")
    public ResponseEntity<Province> getProvinceById(@PathVariable("id") String id) {
        try {
            int provinceId = Integer.parseInt(id);
            Province province = provinceService.getProvinceById(provinceId);
            return new ResponseEntity<>(province, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/province")
    public ResponseEntity<Province> createProvince(@RequestBody Province province) {
        try {
            Province newProvince = provinceService.createProvince(province);
            return new ResponseEntity<>(newProvince, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/province/{id}")
    public ResponseEntity<Province> updateProvince(@PathVariable("id") int id, @RequestBody Province province) {
        try {
            Province updatedProvince = provinceService.updateProvince(id, province);
            if (updatedProvince != null) {
                return new ResponseEntity<>(updatedProvince, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/province/{id}")
    public ResponseEntity<HttpStatus> deleteProvince(@PathVariable("id") int id) {
        try {
            provinceService.deleteProvince(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.provinceapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.devcamp.provinceapi.models.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    public Page<District> findByProvinceId(@Param("provinceId") int provinceId, Pageable pageable);

}

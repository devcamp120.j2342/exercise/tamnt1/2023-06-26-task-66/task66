package com.devcamp.provinceapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {
    public List<Province> findByNameIs(String name);
}

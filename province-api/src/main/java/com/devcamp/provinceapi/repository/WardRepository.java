package com.devcamp.provinceapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.devcamp.provinceapi.models.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {
    public Page<Ward> findByDistrictId(@Param("districtId") int districtId, Pageable pageable);

}
